var SPI = require('spi')

var temperature = process.argv[2]

/*
var spi00 = new SPI.Spi('/dev/spidev1.0', {
  'mode':SPI.MODE['MODE_0'], 
  'chipSelect':SPI.CS['low']
}, function(s){s.open()})

var spi01 = new SPI.Spi('/dev/spidev2.0', {
  'mode':SPI.MODE['MODE_0'], 
  'chipSelect':SPI.CS['low']
}, function(s){s.open()})

var spi10 = new SPI.Spi('/dev/spidev2.1', {
  'mode':SPI.MODE['MODE_0'], 
  'chipSelect':SPI.CS['low']
}, function(s){s.open()})
*/

var spi11 = new SPI.Spi('/dev/spidev2.2', {
  'mode':SPI.MODE['MODE_0'], 
  'chipSelect':SPI.CS['low']
}, function(s){s.open()})


function temperatureToMilliamps(temperature) {
  var milliamps = (0.089*temperature) + 1.152
  return milliamps
}

// 4-20mA click board performs linear conversion of input number from range 800 - 4095 
// into current in range 4mA - 20mA.
function milliampsToMikroRange(milliamps) {
  var mikro = (205.9375*milliamps) - 23.75
  return mikro
}

var milli  = temperatureToMilliamps(temperature)
var mikro = milliampsToMikroRange(milli)

console.log(milli)
console.log(mikro)

var templ  = mikro & 0xff
var temph  = (mikro >> 8) & 0x0f
    temph |= 0x30

console.log(temph.toString(16));
console.log(templ.toString(16));

//spi00.transfer(new Buffer([temph, templ]), new Buffer([0x00, 0x00]))
//spi01.transfer(new Buffer([temph, templ]), new Buffer([0x00, 0x00]))
//spi10.transfer(new Buffer([temph, templ]), new Buffer([0x00, 0x00]))
spi11.transfer(new Buffer([temph, templ]), new Buffer([0x00, 0x00]))

