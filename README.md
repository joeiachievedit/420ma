### Disclaimer ###
This repository is strictly for users with a BeagleBone Black, mikroBUS Cape, and mikro 4-20 Transmitter clickboards.

### Compiling DTS ###
cd 420ma
make dtbo

Edit /boot/uEnv.txt and find "Example v4.1.x" and change to:

##Example v4.1.x
cape_disable=bone_capemgr.disable_partno=BB-BONELT-HDMI,BB-BONELT-HDMIN
#cape_enable=bone_capemgr.enable_partno=
reboot

After rebooting:
echo BB-420MA-01 > /sys/devices/platform/bone_capemgr/slots 

Verify with ls -l /dev/spi* that the SPI devices were created.

### Using NodeJS 420ma ###
cd 420ma

Because NodeJS on Debian 8.2 is still behind the times, install node 0.12 with 

make prereq

Then, do a 

make npm