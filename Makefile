npm:
	npm install

dtbo: BB-420MA-01-00A0.dts
	dtc -O dtb -o BB-420MA-01-00A0.dtbo -b 0 -@ BB-420MA-01-00A0.dts 
	cp BB-420MA-01-00A0.dtbo /lib/firmware

dtbo2: BB-JOEGPIO-01-00A0.dts
	dtc -O dtb -o BB-JOEGPIO-01-00A0.dtbo -b 0 -@ BB-JOEGPIO-01-00A0.dts 
	cp BB-JOEGPIO-01-00A0.dtbo /lib/firmware

install:
	echo BB-420MA-01 > /sys/devices/platform/bone_capemgr/slots
